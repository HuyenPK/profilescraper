import login as lg
import extract as et
import const as const
import urllib.request as url
import bs4 as soup
import html.parser
import time

from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def search_keywords(browser, wait, search_keywords, search_option):
    wait.until(EC.presence_of_element_located((By.ID, "extended-nav-search")))
    wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "artdeco-typeahead-deprecated-input")))
    wait.until(EC.presence_of_element_located((By.XPATH, "//input[@role='combobox']"))).send_keys(search_keywords)
    search_form = browser.find_element_by_xpath("//input[@role='combobox']")
    search_form.send_keys(Keys.RETURN)
    return browser


def get_result_list(browser, wait):
    'Get a list of elements from result page.'
    wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "li.search-result")))
    results = browser.find_elements_by_css_selector("li.search-result")
    return results

def get_profile_links(browser, wait):
    'Get all profile links by scrolling down.'
    links = []
    height = 200
    while(height < browser.execute_script("return document.body.scrollHeight")):
        browser.execute_script("window.scrollTo(0, '%d');" % (height))
        get_profile_links_half(browser, wait, links)
        height += 200
    return links

def get_profile_links_half(browser, wait, links):
    'Get partial results from the page, because some results have contents occluded by javascript and only visible when scrolling down.'
    wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, "a.search-result__result-link")))
    results = browser.find_elements_by_css_selector("a.search-result__result-link")
    half = []
    browser.implicitly_wait(5)
    for res in results:
        tag = res.get_attribute('href')
        if not half.__contains__(tag):
            half.append(tag)
    for url in half:
        if not links.__contains__(url):
            links.append(url)

    return links

def go_to_next_page(browser, wait):
    wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "button.next")))
    browser.find_element_by_css_selector("button.next").click()