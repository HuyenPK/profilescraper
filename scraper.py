import login as lg
import const as const

def crawl_links(browser, wait, site_option):
    config = lg.get_config_file()
    path = config[const.FILE_PATHS][site_option][const.PROFILE_LINKS]
    # lg.load_cookies(site_option, browser)
    with open(path) as f:
        for line in f:
            lg.load_cookies(site_option, browser) #either load here or above?
            browser.get(line)