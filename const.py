"""Configuration constants."""
AUTH = "Auth" #Authentication root key
FILE_PATHS = "Files"
COOKIES_PATH = "Cookies"


CONFPATH = "priv_config.json"
GLOBAL_CONF = "global_config.json"


"""Search site options"""
LINKEDIN = "LinkedIn"
APEC = "Apec"

"""Browser mode"""
BROWSER_HEADLESS = "browser_headless"
BROWSER_UI = "browser_ui"


"""Elements"""
ELEMENTS_CREDS = "creds"
ELEMENTID_LOGIN = "elementid_login"
ELEMENTID_PASSWORD = "elementid_password"
ELEMENTID_URL = "url"
PROFILE_LINKS = "profile_links"
LOGIN = "login" #key
PASSWORD = "password" #value

"""Search option"""
SEARCH_PEOPLE = "search_people"
SEARCH_JOBS = "search_jobs"
SEARCH_COMPANIES = "search_companies"
