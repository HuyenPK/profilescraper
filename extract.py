import const as const
import login as lg

from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

site_elements = {}

def search(browser_option, search_keywords, search_option):
    browser = lg.navigate(const.LINKEDIN, browser_option)
    # browser.implicitly_wait(2)
    # search_form = browser.find_element_by_id("extended-nav-search")
    wait = WebDriverWait(browser, 10)
    search_form = wait.until(EC.presence_of_element_located((By.ID, "extended-nav-search")))
    search_form = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "artdeco-typeahead-deprecated-input")))
    search_form = wait.until(EC.presence_of_element_located((By.XPATH, "//input[@role='combobox']"))).send_keys(search_keywords)
    search_form = wait.until(EC.element_to_be_clickable((By.XPATH, "//button[@data-control-name='nav.search_button']"))).click()
    print(search_form)
    # search_form.find_element_by_xpath("//ul[@class='search-typeahead-v2__entry-point-list']").find_element_by_id("entry-point-result-0").click()
    # name = browser.find_element_by_class_name("pv-top-card-section__name")
    # print(name.text)

def extract_name_linkedin(browser, wait):
    name = wait.until(EC.presence_of_element_located((By.CLASS_NAME, "pv-top-card-v2-section__info")))
    name = wait.until(EC.presence_of_element_located((By.XPATH, "//h1[@class='pv-top-card-section__name']")))


# browser = lg.navigate(const.LINKEDIN, const.BROWSER_UI)
# search(const.BROWSER_UI, "melanie", const.SEARCH_PEOPLE)
