import re
import json
import pickle
import const as const

from selenium.webdriver.firefox.options import Options
from selenium.webdriver import Firefox


def get_brower_headless():
    'Create browser without graphical interface.'
    opts = Options()
    opts.set_headless()
    return Firefox(options=opts)

def get_browser_UI():
    'Create browser with graphical interface.'
    browser = Firefox()
    return browser

def terminate_navigation(browser):
    browser.close()

def get_config_file():
    config_file = open(const.CONFPATH)
    config = json.load(config_file)
    return config

def get_auth(site_option):
    'Retrieve authentications from config.json'
    config = get_config_file()
    login = config[const.AUTH][site_option][const.LOGIN]
    password = config[const.AUTH][site_option][const.PASSWORD]
    return (login,password)

def get_site_creds(site_option):
    'Retrieve configurations related to page.'
    config_file = open(const.GLOBAL_CONF)
    config = json.load(config_file)
    url = config[site_option][const.ELEMENTID_URL]
    login_element = config[site_option][const.ELEMENTS_CREDS][const.ELEMENTID_LOGIN]
    password_element = config[site_option][const.ELEMENTS_CREDS][const.ELEMENTID_PASSWORD]
    return (url, login_element, password_element)

def authenticate(site_option, browser):
    cred = get_auth(site_option)
    login = cred[0]
    password = cred[1]
    conf = get_site_creds(site_option)
    login_element=conf[1]
    password_element=conf[2]
    login_form = browser.find_element_by_id(login_element)
    login_form.send_keys(login)
    login_form = browser.find_element_by_id(password_element)
    login_form.send_keys(password)
    login_form.submit()

def login(site_option, browser_option):
    'Open browser, navigate to url and authenticate'
    if(browser_option == const.BROWSER_HEADLESS):
        browser = get_brower_headless()
    else:
        browser = get_browser_UI()

    conf = get_site_creds(site_option)
    url = conf[0]
    browser.get(url)
    authenticate(site_option=site_option, browser=browser)
    return browser

def save_cookies(site_option, browser):
    config = get_config_file()
    cookie_path = config[const.COOKIES_PATH][site_option]
    pickle.dump(browser.get_cookies(), open(cookie_path, "wb"))

def load_cookies(site_option, browser):
    config = get_config_file()
    cookie_path = config[const.COOKIES_PATH][site_option]
    for cookie in pickle.load(open(cookie_path, "rb")):
        browser.add_cookie(cookie)
    return browser